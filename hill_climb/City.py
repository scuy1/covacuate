class City:
    def __init__(self, name, covid_data):
        self.name = name
        self.covid_data = covid_data
        self.dist = 0
        self.dist_from_origin = 0

    def get_name(self):
        return self.name

    def set_dist_from_input_city(self, dist):
        self.dist = dist

    def get_dist_from_input_city(self):
        return self.dist

    def set_dist_from_origin(self, dist):
        self.dist_from_origin = dist

    def get_dist_from_origin(self):
        return self.dist_from_origin

    def get_covid_data(self):
        return self.covid_data

    def fn(self):
        data_konfirmasi=int(self.get_covid_data()['Terkonfirmasi'])
        data_sembuh=int(self.get_covid_data()['Sembuh'])
        data_meninggal=int(self.get_covid_data()['Meninggal'])
        return (data_konfirmasi-data_sembuh-data_meninggal) + int(self.dist)*0.0005

    def __str__(self):
        return "{}, COVID-19: {}, jarak dari kota asal: {} , heuristik (hn) : {}, jarak : {}".format(self.name, self.covid_data, self.dist, self.fn(), self.get_dist_from_input_city())