import csv, requests, os, sys
import hill_climb.City
from hill_climb.City import City

class Province:
    def __init__(self, name):
        self.name = name
        # self.cities = []
        self.cities = {}
        # self.origin_city = ""

    def get_name(self):
        return self.name

    def get_cities(self):
        return self.cities

    def get_cities_as_list(self):
        tmp_lst = []
        for city_name,city in self.get_cities().items():
            tmp_lst.append(city)
        return tmp_lst

    def get_city(self, city_name):
        return self.cities.get(city_name)

    def add_city(self, city_name, city):
        self.cities[city_name] = city

    def print_list_city(self):
        print("List kota-kota yang berada di provinsi {}:".format(self.name))
        for num, city in enumerate(self.cities, start=1):
            print("{}. {}".format(num, city.get_city_name()))

    def get_city_from_csv(self):
        workpath = os.path.dirname(os.path.abspath(__file__))
        csv_file = os.path.join(workpath,'static/hill_climb/data_covid/{}.csv'.format(self.name))
        with open(csv_file) as province_csv:
            csv_reader = csv.reader(province_csv, delimiter=',')
            next(csv_reader)
            for row in csv_reader:
                city_name = row[2]
                covid_data = {'Terkonfirmasi':row[3],'Sembuh':row[4], 'Meninggal':row[5]}
                city = City(city_name,covid_data)

                self.add_city(city_name, city)

    def __str__(self):
        str_repr = "Provinsi {}\nDaftar Kota:\n".format(self.name)
        counter = 1
        for city in self.cities:
            str_repr += "[{}]{}\n".format(counter, city)
            counter += 1
        return str_repr
