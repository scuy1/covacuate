from django.urls import path
from .views import homepage, show_map, get_city, about, get_safest_city

app_name = 'hill_climb'

urlpatterns = [
    path('', homepage, name='home'),
    path('evacuate/', show_map, name='map'),
    path('evacuate/<province>', get_city, name='get_city'),
    path('get_safest_city/', get_safest_city, name='get_safest_city'),
    path('about/', about, name='about'),
]
