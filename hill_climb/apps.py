from django.apps import AppConfig


class HillClimbConfig(AppConfig):
    name = 'hill_climb'
