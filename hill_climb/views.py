from hill_climb import HillClimb, coVacuate
from hill_climb.HillClimb import HillClimb
from django.http.response import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def homepage(request):
    return render(request, 'homepage.html', {})

def show_map(request):
    return render(request, 'evacuate.html', {})

def get_city(request, province):
    city_list = coVacuate.get_city_of_province(province)
    return JsonResponse({
        'province':province,
        'cities':city_list
        })

def about(request):
    return render(request, 'about.html', {})

def get_safest_city(request):
    province_name = request.GET.get('province_name')
    print("INI"+province_name)
    city_name = request.GET.get('city_name')
    print("INI"+city_name)
    # city_name = request.POST['city_name']
    hc = HillClimb()
    result = hc.get_safest_city(province_name,city_name)
    response_data = {}
    index = 0
    list_result = result[0]
    origin_data = result[1].get_covid_data()
    to_json_origin = {}
    to_json_origin['city'] = result[1].get_name()
    to_json_origin['prov'] = result[2].get_name()
    to_json_origin['konfirmasi'] = origin_data['Terkonfirmasi']
    to_json_origin['sembuh'] = origin_data['Sembuh']
    to_json_origin['meninggal'] = origin_data['Meninggal']
    response_data['origin'] = to_json_origin

    if len(list_result) <= 5:
        max_len_list_result = len(list_result)
    else:
        max_len_list_result = 5

    for i in range(max_len_list_result):
        el = list_result[i]
        el_to_json = {}

        city = el[1]
        data_covid_city = city.get_covid_data()
        el_to_json['city'] = city.get_name()
        el_to_json['prov'] = el[2].get_name()
        el_to_json['dist'] = city.get_dist_from_origin()
        el_to_json['konfirmasi'] = data_covid_city['Terkonfirmasi']
        el_to_json['sembuh'] = data_covid_city['Sembuh']
        el_to_json['meninggal'] = data_covid_city['Meninggal']

        response_data[index] = el_to_json
        index += 1
    response_data['count'] = index
    print("udahkeluar")
    print(response_data)
    return JsonResponse(response_data)
