import csv, requests, json, os, sys

class City:
    def __init__(self, name, covid_data):
        self.name = name
        self.covid_data = covid_data
        self.dist = 0

    def get_city_name(self):
        return self.name

    def set_dist_from_input_city(self, dist):
        self.dist = dist

    def get_dist_from_input_city(self):
        return self.dist

    def get_covid_data(self):
        return self.covid_data

    def fn(self):
        return int((self.get_covid_data())['Terkonfirmasi'])*0.1 + int(self.dist)*0.0005

    def __str__(self):
        return "{}, COVID-19: {}, jarak dari kota asal: {} , heuristik (hn) : {}, jarak : {}".format(self.name, self.covid_data, self.dist, self.fn(), self.get_dist_from_input_city())


class Province:
    def __init__(self, name):
        self.name = name
        self.cities = []
        self.origin_city = ""

    def get_origin_city(self):
        for i in range(len(self.cities)):
            # print("Kota ke "+str(i)+" "+self.cities[i].get_city_name())
            if self.cities[i].get_city_name() == self.origin_city:
                return self.cities[i]

    def add_city(self, city):
        self.cities.append(city)

    def print_list_city(self):
        print("List kota-kota yang berada di provinsi {}:".format(self.name))
        for num, city in enumerate(self.cities, start=1):
            print("{}. {}".format(num, city.get_city_name()))

    def get_city_from_csv(self):
        workpath = os.path.dirname(os.path.abspath(__file__))
        csv_file = os.path.join(workpath,'static/hill_climb/data_covid/{}.csv'.format(self.name))
        with open(csv_file) as province_csv:
            csv_reader = csv.reader(province_csv, delimiter=',')
            next(csv_reader)
            for row in csv_reader:
                city_name = row[2]
                covid_data = {'Terkonfirmasi':row[3],'Sembuh':row[4], 'Meninggal':row[5]}
                city = City(city_name,covid_data)

                self.add_city(city)

    def api_call(self, origin_city):
        origin_sentence = "origins={}+{}".format(origin_city, self.name.replace(" ","+"))

        destination_sentence = "destinations={}+{}".format(self.cities[0].name.replace(" ","+"), self.name.replace(" ","+"))
        for i in range(1, len(self.cities)):
            destination_sentence += "|{}+{}".format(self.cities[i].name.replace(" ","+"), self.name.replace(" ","+"))

        key_sentence = "key=AIzaSyCD6KGRF2RkIeq27NWUY2cqZFstgWXgqC4"
        api_sentence = "https://maps.googleapis.com/maps/api/distancematrix/json?{}&{}&{}".format(origin_sentence, destination_sentence, key_sentence)
        
        ############debugging#############
        # print(api_sentence)
        # print()

        api_result = requests.get(api_sentence)
        return api_result.json()

    def get_city_dist(self, origin_city):
        self.origin_city = origin_city
        city_dist_dict = self.api_call(origin_city)

        for i in range(0, len(self.cities)):
            city = self.cities[i]
            dist_tuple = city_dist_dict["rows"][0]["elements"][i]
            status_of_counted_dist = dist_tuple["status"]
            dist = dist_tuple["distance"]["value"] if status_of_counted_dist == "OK" else sys.maxsize
            city.set_dist_from_input_city(dist)

    def __str__(self):
        str_repr = "Provinsi {}\nDaftar Kota:\n".format(self.name)
        counter = 1
        for city in self.cities:
            str_repr += "[{}]{}\n".format(counter, city)
            counter += 1
        return str_repr

    def get_cities(self):
        return self.cities
    
def print_list_province():
    workpath = os.path.dirname(os.path.abspath(__file__))
    province_path = os.path.join(workpath,'static/hill_climb/data_covid/')
    province_list = [os.path.splitext(filename)[0] for filename in os.listdir(province_path)]
    # province_path = "DataCovid/"
    # province_list = [os.path.splitext(filename)[0] for filename in os.listdir(province_path)]
    print("List provinsi:")
    for num, province in enumerate(province_list, start=1):
        print("{}. {}".format(num, province))


def remove_current_neighbour(list_of_neighbours):
    remove_neighbours = []
    for c in list_of_neighbours:
        # print(c)
        if c.dist > 1000000 or c.dist == 0:
            # print("Removing!")
            remove_neighbours.append(c)
    return remove_neighbours


def get_best_neighbour(list_of_neighbours):
    print(list_of_neighbours)
    best_neighbour_city = list_of_neighbours[0]
    
    for i in range(1, len(list_of_neighbours)):
        city = list_of_neighbours[i]
        if city.fn() < best_neighbour_city.fn():
            best_neighbour_city = city
    
    return best_neighbour_city


def hill_climb(province, max_loop):
    import random
    prov = province
    current_city = prov.get_origin_city()
    print("Kota")
    print(current_city)
    list_of_current_neighbour = prov.get_cities().copy()
    print("Tetangga")
    print(list_of_current_neighbour)

    for i in range(0, max_loop):
        if i > 0:
            list_of_current_neighbour = []
            list_of_current_neighbour = prov.get_cities().copy()
            prov.get_city_dist(current_city.get_city_name())
        
        print("--------")
        for c in prov.get_cities():
            print(c)
        print("--------")
        
        remove_neighbours = remove_current_neighbour(list_of_current_neighbour)
        
        for c in remove_neighbours:
            list_of_current_neighbour.remove(c)

        print("--------")
        for c in list_of_current_neighbour:
            print(c)
        print("--------")

        if list_of_current_neighbour == [] :
            return current_city
            
        best_neighbour = get_best_neighbour(list_of_current_neighbour)
        print("Best neighbour: {}".format(best_neighbour.get_city_name()))
  

        # print(current_city.get_covid_data()["Terkonfirmasi"])
        # print(best_neighbour.get_covid_data()["Terkonfirmasi"])
        
        # Komposisi heuristik ubah disini
        if current_city.fn() <= best_neighbour.fn() :
            return current_city
        else:
            current_city = best_neighbour
        
        print("-------")
        print(current_city)
        print("-------")
    return current_city

    

def get_safe_city(province_name,city_name):
    # print("DAHMASUK")
    # print_list_province()
    print(province_name)
    print(city_name)
    province = Province(province_name)
    province.get_city_from_csv()
    # print("atas udah")
    # province.print_list_city()
    # origin_city = input(city_name)
    province.get_city_dist(city_name)
    # print("2 udahss")
    kota_terdekat = hill_climb(province, 5)
    # print("Kota teraman: {}!".format(kota_terdekat.get_city_name()))
    return kota_terdekat.get_city_name()
    
def get_city_of_province(initial_province):
    province = Province(initial_province)
    province.get_city_from_csv()
    city_list = []
    for city in province.get_cities():
        city_name = city.get_city_name()
        city_list.append(city_name)
    return city_list


# if __name__ == '__main__':
#     main()