import hill_climb.City, hill_climb.Province, hill_climb.ProvinceDB
import json, os, requests, sys
import random
from hill_climb.ProvinceDB import ProvinceDB
import queue

class HillClimb:
    def __init__(self):
        self.province_db = ProvinceDB()
        self.province_db.add_provinces_to_db()

    def get_safest_city(self, origin_province="", origin_city="", max_loop=1):
        current_province = self.province_db.get_province(origin_province)
        current_city = current_province.get_city(origin_city)
        current_origin_city = current_city
        current_origin_province = current_province
        #current_cities_sample_space = self.get_city_sample_space(current_province) 
        prov_sample_space = [current_province]
        prov_sample_space += self.get_current_prov_neighbours(current_province)
        self.get_neighbours_dist_to_origin(current_province, current_city, prov_sample_space) 
        result = []

        for i in range(0, max_loop):
            prov_sample_space = [current_province]
            prov_sample_space += self.get_current_prov_neighbours(current_province)
            self.get_neighbours_dist_to_current(current_province, current_city, prov_sample_space)

            cities_with_province = self.get_cities_with_province(prov_sample_space)
            for (city, prov) in cities_with_province:
                if ((city.get_dist_from_input_city() > 99999) or (city.get_dist_from_input_city() == 0)):
                    cities_with_province.remove((city, prov))

            best_neighbour_pq = self.get_best_neighbour(cities_with_province)
            best_neighbour = best_neighbour_pq.queue[0][1]
            best_province = best_neighbour_pq.queue[0][2]

            while not best_neighbour_pq.empty():
                result.append(best_neighbour_pq.get())

            if current_city.fn() <= best_neighbour.fn():
                break
            current_city = best_neighbour
            current_province = best_province
        # remove duplicate result
        result = list(dict.fromkeys(result))
        result.sort()
        # safest_city, city_in_province = self.random_restart(prov_sample_space, max_loop)
        # result = self.get_best_neighbour_city(prov_sample_space)
        return (result, current_origin_city, current_origin_province)

    def get_best_neighbour(self, cities_with_province):
        pq_result = queue.PriorityQueue()
        best_neighbour_city = cities_with_province[0][0]
        city_in_province = cities_with_province[0][1]
        for i in range(1, len(cities_with_province)):
            city = cities_with_province[i][0]
            province = cities_with_province[i][1]
            if city.fn() < best_neighbour_city.fn():
                best_neighbour_city = city
                city_in_province = province
                pq_result.put((best_neighbour_city.fn(), best_neighbour_city, city_in_province))  

        if pq_result.empty():
            pq_result.put((best_neighbour_city.fn(), best_neighbour_city, city_in_province))   
        return pq_result

    def get_cities_with_province(self, prov_sample_space):
        result = []
        for prov in prov_sample_space:
            cities = prov.get_cities()
            for city_name, city in cities.items():
                result.append((city, prov))
        return result

    def get_best_neighbour_city(self, province_space):
        pq_result = queue.PriorityQueue()
        first_province_in_list = province_space[0]
        first_city_in_list = first_province_in_list.get_cities_as_list().pop(0)
        city_in_province = first_province_in_list
        best_neighbour_city = first_city_in_list

        for province in province_space:
            cities = province.get_cities()
            for city_name, city in cities.items():
                # Debug
                # print("{}; dist {} and {}; Current best {} fn {}; Current city {} fn {}".format(province.get_name(), best_neighbour_city.get_dist_from_input_city(), city.get_dist_from_input_city(), best_neighbour_city.get_name(), best_neighbour_city.fn(), city.get_name(), city.fn()))
                if city.fn() < best_neighbour_city.fn():
                    city_in_province = province
                    best_neighbour_city = city
                    pq_result.put((best_neighbour_city.fn(), best_neighbour_city, city_in_province))

        if pq_result.empty():
            pq_result.put((best_neighbour_city.fn(), best_neighbour_city, city_in_province))

        return pq_result

    def random_restart(self, province_space, max_loop):
        province_table = []
        all_cities = []
        for province in province_space:
            province_table.append(province)      
            cities = province.get_cities()      
            for city_name, city in cities.items():
                province_table.append(province)
                all_cities.append(city)

        hc_result = []
        for i in range(max_loop):
            hc = self.random_hc(all_cities, province_table)
            hc_result.append(hc)

        best_city, best_province = hc_result.pop(0)
        for city, province in hc_result:
            # print("Current best {} fn {}; Current city {} fn {}".format(best_city.get_name(), best_city.fn(), city.get_name(), city.fn()))
            if city.fn() < best_city.fn():
                best_city = city
                best_province = province
        return best_city, best_province

    def random_hc(self, all_cities, province_table):
        city_count = len(all_cities)
        random_start = random.randint(0, (city_count-1))
        # print("Jumlah kota {}; Nilai random {}".format(city_count, random_start))

        best_neighbour_city = all_cities[random_start]
        for i in range(random_start, len(all_cities)):
            city = all_cities[i]
            # print("dist {} and {}; Current best {} fn {}; Current city {} fn {}".format(best_neighbour_city.get_dist_from_input_city(), city.get_dist_from_input_city(), best_neighbour_city.get_name(), best_neighbour_city.fn(), city.get_name(), city.fn()))
            if city.fn() <= best_neighbour_city.fn():
                best_neighbour_city = city
            else:
                break
        return best_neighbour_city, province_table[i]

    def get_current_prov_neighbours(self, province):
        neighbour_provs = self.province_db.get_neighbour_of_province(
                province.get_name()
            )

        current_province_neighbour = []
        for neighbour in neighbour_provs:
            tmp_prov = self.province_db.get_province(neighbour)
            current_province_neighbour.append(tmp_prov)
        
        return current_province_neighbour

    def get_neighbours_dist_to_current(self, current_prov, current_city, sample_space_provs):
        
        for prov in sample_space_provs:
            neighbour_dist_data = self.api_call(current_prov, current_city, prov)
            distance_data = neighbour_dist_data["rows"][0]["elements"]
            cities = prov.get_cities()
            city_count = 0
            for city_name, city in cities.items():
                # print("{} : {}".format(prov.get_name(), city.get_name()))
                dist_tuple = distance_data[city_count]
                status = dist_tuple["status"]
                dist = dist_tuple["distance"]["value"] if status == "OK" else sys.maxsize
                city.set_dist_from_input_city(dist)
                city_count += 1

        # Debug
        """for prov in sample_space_provs:
            cities = prov.get_cities()
            for city_name, city in cities.items():
                print("{} : {} : {}".format(prov.get_name(), city.get_name(), city.get_dist_from_input_city()))"""

    def get_neighbours_dist_to_origin(self, current_prov, current_city, sample_space_provs):
        
        for prov in sample_space_provs:
            neighbour_dist_data = self.api_call(current_prov, current_city, prov)
            distance_data = neighbour_dist_data["rows"][0]["elements"]
            cities = prov.get_cities()
            city_count = 0
            for city_name, city in cities.items():
                # print("{} : {}".format(prov.get_name(), city.get_name()))
                dist_tuple = distance_data[city_count]
                status = dist_tuple["status"]
                dist = dist_tuple["distance"]["value"] if status == "OK" else sys.maxsize
                city.set_dist_from_origin(dist)
                city_count += 1

    def api_call(self, origin_prov, current_city, current_prov):
        origin_sentence = "origins={}+{}".format(
            current_city.get_name().replace(" ","+"), 
            origin_prov.get_name().replace(" ","+")
        )

        destination_sentence = ""
        first_city = True
        prov_cities = current_prov.get_cities()
        for city_name, city in prov_cities.items():
            #print("{} : {}.".format(prov.get_name(), city.get_name()))
            tmp_prov_name = current_prov.get_name().replace(" ", "+")
            tmp_city_name = city.get_name().replace(" ", "+")

            if not first_city:
                destination_sentence += "|{}+{}".format(tmp_city_name, tmp_prov_name)
            else:
                first_city = False
                destination_sentence += "destinations={}+{}".format(tmp_city_name, tmp_prov_name)
        
        key_sentence = "key=AIzaSyCD6KGRF2RkIeq27NWUY2cqZFstgWXgqC4"
        api_sentence = "https://maps.googleapis.com/maps/api/distancematrix/json?{}&{}&{}".format(origin_sentence, destination_sentence, key_sentence)

        neighbours_dist = requests.get(api_sentence)

        return neighbours_dist.json()          
    

if __name__ == '__main__':
    tmpHC = HillClimb()
    # print(tmpHC.get_neighbour_of_province("Jawa Barat"))
    tmpHC.get_safest_city("DKI Jakarta", "Jakarta Selatan")