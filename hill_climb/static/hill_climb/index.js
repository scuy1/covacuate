function getValue(province,city) {
    // alert("Maasuk")
    // var province_name = document.getElementById(province).innerHTML;
    var a = document.getElementById(province).value;
    var e = document.getElementById(city);
    var strUser = e.options[e.selectedIndex].value;
    emptyResultPlaceholder();
    sendCities(String(a),String(strUser));
};

function sendCities(province,city) {
    $.ajax({
        type : "GET",
        url : "/get_safest_city/",
        data : {
            'province_name': province,
            'city_name': city,
        },
        success : function (result) {
            var safest = result[0];
            var origin = result['origin']
            $("#safestCity").append(safest['city']);
            $("#safestProv").append(safest['prov']);
            if (safest['city'] == origin['city']) {
                $("#sameCity").append("Great! You are already in the safest city.");
            }

            // change element in DETAILS section
            $("#originCiti").append(origin['city']);
            $("#originProvince").append(origin['prov']);
            $("#originConfirmed").append(origin['konfirmasi']);
            $("#originRecovered").append(origin['sembuh']);
            $("#originDeaths").append(origin['meninggal']);
            $("#rsltCity").append(safest['city']);
            $("#rsltProvince").append(safest['prov']);
            $("#rsltConfirmed").append(safest['konfirmasi']);
            $("#rsltRecovered").append(safest['sembuh']);
            $("#rsltDeath").append(safest['meninggal']);
            $("#rsltDistance").append(safest['dist']);

            // change element in POTENTIAL CITIES section
            var count = result['count'];
            for (let i = 0; i < count; i++) {
                var city = result[i]
                var city_entry = "<tr><td>" + (i+1) + "</td><td>" + city['city'] + "</td><td>" + city['prov'] + "</td><td>" + city['konfirmasi'] + "</td><td>" + city['sembuh'] + "</td><td>" + city['meninggal'] + "</td><td>" + city['dist'] + "</td></tr>";
                $('#potentialCities').append(city_entry);
            }
            if (count == 1) {
                $("#noOtherPotential").append("No other potential cities available :(");
            }
                
        }
    })
};

function emptyResultPlaceholder() {
    $("#safestCity").empty();
    $("#safestProv").empty();
    $("#originCiti").empty();
    $("#originProvince").empty();
    $("#originConfirmed").empty();
    $("#originRecovered").empty();
    $("#originDeaths").empty();
    $("#rsltCity").empty();
    $("#rsltProvince").empty();
    $("#rsltConfirmed").empty();
    $("#rsltRecovered").empty();
    $("#rsltDeath").empty();
    $("#rsltDistance").empty();
    $('#potentialCities').empty();
    $("#noOtherPotential").empty();
    $("#sameCity").empty();
}

function change_display_province(province_name) {
    $("#cities").empty();
    // $("#province").html(province_name);
    document.getElementById("province_form").value=province_name; 
    get_cities_of_province(province_name)
};

function get_cities_of_province(province_name) {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/evacuate/" + province_name,
        dataType: "json",
        success: function(data) {
            console.log(data);
            if (data.province && data.cities) {
                let cities = data.cities;
                city_selection = $("#cities");
                $.each(cities, function(i,name) {
                    city_selection
                        .append($("<option></option>")
                        .attr("value",name)
                        .text(name));
                });
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
}

$("#province_form").mouseup(function() {
    var province_form = document.getElementById("province_form");
    var province_val = province_form.value;
    change_display_province(province_val);
})