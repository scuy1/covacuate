import hill_climb.Province
import os, json
from hill_climb.Province import Province

class ProvinceDB:
    def __init__(self):
        self.province_data = {}
        self.neighbours_of = neighbour_json_data()

    def get_province(self, province_name):
        return self.province_data.get(province_name)

    def get_neighbour_of_province(self, province):
        return self.neighbours_of.get(province)

    def add_provinces_to_db(self):
        workpath = os.path.dirname(os.path.abspath(__file__))
        province_path = os.path.join(workpath, 'static/hill_climb/data_covid/')
        province_names = [os.path.splitext(filename)[0] for filename in os.listdir(province_path)]

        for province_name in province_names:
            # print(province_name)
            tmpProv = Province(province_name)
            tmpProv.get_city_from_csv()
            self.province_data[province_name] = tmpProv
        '''
        for name,value in self.province_data.items():
            print(value)
        '''

    def check(self):
        for name,prov in self.province_data.items():
            print("{} : {} cities.".format(name,len(prov.get_cities())))
            for city_name,city in prov.get_cities().items():
                if city:
                    print(city.get_name())
                else:
                    print("Error: {}.".format(name))

def neighbour_json_data():
    workpath = os.path.dirname(os.path.abspath(__file__))
    json_file_path = os.path.join(workpath,'static/hill_climb/neighbour_province.json')
    neighbour_json = open(json_file_path)

    json_data = json.load(neighbour_json)

    neighbour_of = json_data['neighbourOf']

    neighbour_json.close()
    
    return neighbour_of


if __name__ == '__main__':
    testProvDB = ProvinceDB()
    testProvDB.add_provinces_to_db()
    testProvDB.check()